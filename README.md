# cms_mobile

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Code of Conduct](https://img.shields.io/badge/%E2%9D%A4-code%20of%20conduct-e04545.svg?style=flat)](https://gitlab.com/amfoss/cms-mobile/-/blob/master/CODE_OF_CONDUCT.md)

cms-mobile is a flutter application for the amFOSS CMS. Using the application, club members can login into the CMS and view club attendence, their profile and status updates. 

## Getting Started

These instructions will get you a copy of the project up and be running on your local machine for development and testing purposes.

### Prerequisites

[Android Studio](https://developer.android.com/studio), with a recent version of the Android SDK.

### Setting up your development environment

- Download and install Git.

- Fork the [cms-mobile project](https://gitlab.com/amfoss/cms-mobile)

- Clone your fork of the project locally. At the command line:
    ```
    $ git clone https://gitlab.com/YOUR-GITLAB-USERNAME/cms-mobile
    ```

If you prefer not to use the command line, you can use Android Studio to create a new project from version control using 
```
https://gitlab.com/YOUR-GITLAB-USERNAME/cms-mobile
```

Open the project in the folder of your clone from Android Studio and build the project. If there are any missing dependencies, install them first by clicking on the links provided by Android studio. Once the project is built successfully, run the project by clicking on the green arrow at the top of the screen.

In order to run the project, you will need the Flutter SKD setup up. If this is your first Flutter project, below are a few useful resources:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Screenshots

<table border="0">
  <tr>
    <td><img src="https://user-images.githubusercontent.com/41234408/78532766-6d2c2280-7805-11ea-822f-06896ffbcd74.png" width="300"></td>
    <td><img src="https://user-images.githubusercontent.com/41234408/78532340-b0d25c80-7804-11ea-92cb-5b3ed6f311db.png" width="300"></td>
    <td><img src="https://user-images.githubusercontent.com/41234408/78532385-c6478680-7804-11ea-9e0c-cc84dd160bad.png" width="300"></td>
  </tr>
  <tr>
    <td><img src="https://user-images.githubusercontent.com/41234408/78532418-d6f7fc80-7804-11ea-95ab-8a8c42562e74.png" width="300"></td>
    <td><img src="https://user-images.githubusercontent.com/41234408/78532471-eecf8080-7804-11ea-8b67-12b4c23a1e90.png" width="300"></td>
    <td><img src="https://user-images.githubusercontent.com/41234408/78532545-10c90300-7805-11ea-8342-3cfd1bb1d190.png" width="300"></td>
  </tr>
</table>

##  Dependencies:

[cupertino_icons](https://pub.dev/packages/cupertino_icons): 0.1.2

[graphql_flutter](https://pub.dev/packages/graphql_flutter): 3.0.1-beta.1

[intl](https://pub.dev/packages/intl): 0.16.1

[moor_flutter](https://pub.dev/packages/moor_flutter): 2.1.1

[url_launcher](https://pub.dev/packages/url_launcher): 5.4.2

[flutter_markdown](https://pub.dev/packages/flutter_markdown): 0.3.4

[html2md](https://pub.dev/packages/html2md): 0.5.1

[date_range_picker](https://pub.dev/packages/date_range_picker): 1.0.6

[fl_chart](https://pub.dev/packages/fl_chart): 0.9.0

[flutter_icons](https://pub.dev/packages/flutter_icons): 1.1.0

[toast](https://pub.dev/packages/toast): 0.1.5

## Get in Touch

To contact us, you can create an issue over [here](https://gitlab.com/amfoss/cms-mobile/-/issues/). 
You can also participate in technical discussions and ask your doubts on the [IRC Freenode](https://webchat.freenode.net/). Just join in and participate in the discussions at the #amfoss channel.

## License
This project is licensed under the [GNU General Public License v3.0](https://gitlab.com/amfoss/TempleApp/blob/master/LICENSE).